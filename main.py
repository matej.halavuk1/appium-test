"""
This module contains a simple class for creation of a session
to initialize a connection via appium test framework
and your test device i.e android/ios
"""

from appium import webdriver


class CreateSession:
    desired_cap = {
        "deviceName": "XEDNW18827002424",
        "platformName": "Android",
        "automationName": "UiAutomator2",
        "app": "C:\\Users\\matej.halavuk\\Downloads\\Greyp_v2.1.3.40_apkpure.com.apk",
        "unicodeKeyboard": "false",
        "resetKeyboard": "false"
    }

    def __init__(self):
        webdriver.Remote("http://localhost:4723/wd/hub", self.desired_cap)



