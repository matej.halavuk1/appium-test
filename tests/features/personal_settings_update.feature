 Feature: Personal Settings Update
 	As a logged in app user,
 	I want to update any of my personal settings,
 	So that I have up to date state.

  Scenario: Change Name of a logged in user
 	Given I have downloaded greyp mobile application
 	When I open greyp mobile application
    And I wait for few seconds until it opens
 	Then I should see main login screen
 	Given I have entered a valid E-mail YYYYYYYY and password XXXXXXX
 	When I press the Login button
 	Then I should be logged into the system
 	Given I have read terms of use
    When I click on Agree button
    Then an Agree button should change color
    Given I have read privacy policy terms of use
    When I click on Agree button
    Then an Agree button should change color
    And Continue button should change color
    Given I have seen Continue button changes its color
    When I click on Continue button
    Then I should be redirected to another page
    Given I have read Location terms of use
    When I click on Agree button
    Then an Agree button should change color
    Given I have read Health Data terms of use
    When I click on Agree button
    Then an Agree button should change color
    Given I have scrolled down the page
    Given I have read Email Notification terms of use
    When I click on Agree button
    Then an Agree button should change color
    Given I have read Data Usage terms of use
    When I click on Agree button
    Then an Agree button should change color
    And Continue button changes its color
    Given I see Continue button chages its color
    When I click on Continue button
    Then I should be redirected to another page
    Given I see I Get It button on the screen
    When I click on I Get It button
    Then I should be redirected to another page
    Given I see SKIP PAIRING button
    When I click on SKIP PARIRING button
    Then I should be redirected to another page
    Given I see a pop-up with allow access to location  of this device
    When I click on Allow button
    Then I should no longer see a pop-up
    Given I see a pop-up with allow access to photos, media, and other of this device
    When I click on Allow button
    Then I should no longer see a pop-up
    Given I see settings button
    When I click on settings button
    Then I should be redirected to another page
    Given I see personal settings label
    When I click on personal settings label
    Then I should be redirected to personal settings details page
    Given I see First Name field
    When I click on First Name field
    And I give First Name value of Test
    Then I should see value of field First Name to be Test
    Given I see back arrow
    When I click on back arrow
    Then I should be redirected back to personal settings page
    Given I see First Name as Test
    When I compare First Name with Test
    Then I should see same Names

